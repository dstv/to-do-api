﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using POCWebApp.DBContext;
using POCWebApp.DBContext.Models;
using POCWebApp.DBContext.Tables;

namespace POCWebApp.Controllers
{
   

    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {

        [HttpGet("GetTasksForUserID/{id}")]
        public ApiResult<ReturnAPI> Get(int id)
        {
            ReturnAPI res = new ReturnAPI();
            try
            {
                res.data = DataService.GetUserTasks(id);
                res.status = status.OK;
            } catch (Exception ex)
            {
                res.status = status.InternalServerError;
                res.message = ex.Message;
                res.data = null;
            }
           
            return new ApiResult<ReturnAPI>(res);
        }


        [HttpGet("GetTasksForUserEmail/{email}")]
        public ApiResult<ReturnAPI> GetTasksForUserEmail(string email)
        {
            ReturnAPI res = new ReturnAPI();
            try
            {
               Users user = DataService.GetUserIDForEMail(email);
                if (user != null)
                {
                    res.data = DataService.GetUserTasks(user.ID);
                    res.status = status.OK;
                } else
                {
                    res.status = status.ContentNotFound;
                    res.message = "User does not exist";
                }
               
            } catch (Exception ex)
            {
                res.status = status.InternalServerError;
                res.message = ex.Message;
                res.data = null;
            }
           
            return new ApiResult<ReturnAPI>(res);
        }

        [HttpGet("AddUserByEmail/{email}")]
        public ApiResult<ReturnAPI> AddUserByEmail(string email)
        {
            ReturnAPI res = new ReturnAPI();
            try
            {
                Users user = DataService.AddUpdateUser(email);
                res.status = status.OK;
                res.data = user;
            }
            catch (Exception ex)
            {
                res.status = status.InternalServerError;
                res.message = ex.Message;
                res.data = null;
            }

            return new ApiResult<ReturnAPI>(res);
        }


        [HttpPost("AddUpdateTaskForUser")]
        public ApiResult<ReturnAPI> AddUpdateTaskForUser([FromBody] Tasks task)
        {
            ReturnAPI res = new ReturnAPI();
            try
            {
                DataService.AddUpdateTaskForUser(task);
                res.status = status.OK;
            }
            catch (Exception ex)
            {
                res.status = status.InternalServerError;
                res.message = ex.Message;
                res.data = null;
            }

            return new ApiResult<ReturnAPI>(res);
        }

        [HttpGet("DeleteTaskByID/{ID}")]
        public ApiResult<ReturnAPI> DeleteTaskByID(int ID)
        {
            ReturnAPI res = new ReturnAPI();
            try
            {
                DataService.DeleteTaskByID(ID);
                res.status = status.OK;
            }
            catch (Exception ex)
            {
                res.status = status.InternalServerError;
                res.message = ex.Message;
                res.data = null;
            }

            return new ApiResult<ReturnAPI>(res);
        }
    }
}