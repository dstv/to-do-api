﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POCWebApp.DBContext.Tables
{
    public class Tasks
    {
        public int ID { get; set; }
        public int UserID { get; set; }
        public string Description { get; set; }
        public bool IsComplete { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ReminderDate { get; set; }
        public bool IsImportant { get; set; }
    }
}
