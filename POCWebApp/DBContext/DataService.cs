﻿using POCWebApp.DBContext.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POCWebApp.DBContext
{
    public class DataService
    {
        public static void Configure()
        {

        }

        public static Users GetUserIDForEMail(string email)
        {
            using (TasksContext db = new TasksContext())
            {
                return db.Users.Where(w => w.UserEmailOrCell == email).FirstOrDefault();
            }
        }

        public static List<Tasks> GetUserTasks(int userID)
        {
            using (TasksContext db = new TasksContext())
            {
                return db.UserTasks
                    .Where(w => w.UserID == userID)
                    .ToList();
            }
        }

        public static Users AddUpdateUser(string email)
        {
            using (TasksContext db = new TasksContext())
            {
                var user = db.Users.Where(b => b.UserEmailOrCell == email).FirstOrDefault();
                if (user == null)
                {
                    user = new Users();
                    user.UserEmailOrCell = email;
                    db.Users.Add(user);
                    user = db.Users.Where(b => b.UserEmailOrCell == email).FirstOrDefault();
                }
                db.SaveChanges();
                return user;
            }

        }

        public static void AddUpdateTaskForUser(Tasks task)
        {
            using (TasksContext db = new TasksContext())
            {
                var dbTask = db.UserTasks.Where(b => b.ID == task.ID).FirstOrDefault();
                if (dbTask == null)
                {
                    db.UserTasks.Add(task);
                } else
                {
                    dbTask.CreatedDate = task.CreatedDate;
                    dbTask.Description = task.Description;
                    dbTask.IsComplete = task.IsComplete;
                    dbTask.ReminderDate = task.ReminderDate;
                    dbTask.UserID = task.UserID;
                    dbTask.IsImportant = task.IsImportant;
                }
                db.SaveChanges();
            }
        }

        public static void DeleteTaskByID(int ID)
        {
            using (TasksContext db = new TasksContext())
            {
                var dbTask = db.UserTasks.Where(b => b.ID == ID).FirstOrDefault();
                if (dbTask != null)
                {
                    db.UserTasks.Remove(dbTask);
                }
                db.SaveChanges();
            }
        }

    }
}
