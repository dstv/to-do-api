﻿using Microsoft.EntityFrameworkCore;
using POCWebApp.DBContext.Tables;
using System.Configuration;
using Microsoft.Extensions.Configuration;
using System;

namespace POCWebApp.DBContext
{
    public class TasksContext : DbContext
    {
        public TasksContext()
        {
        }

        public TasksContext(DbContextOptions<TasksContext> options)
            : base(options)
        {

        }

        public virtual DbSet<Users> Users { get; set; }
        public virtual DbSet<Tasks> UserTasks { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //optionsBuilder.UseSqlServer(ConfigurationManager.ConnectionStrings["CSPConnectionString"].ConnectionString);
                IConfigurationRoot _configuration = new ConfigurationBuilder()
                             .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                             .AddJsonFile("appsettings.json")
                             .Build();
                optionsBuilder.UseSqlServer(_configuration.GetConnectionString("DB"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Users>(entity =>
            {
                entity.Property(e => e.UserEmailOrCell)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

        }
    }
}
