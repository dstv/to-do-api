﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POCWebApp.DBContext.Models
{
    public class ReturnAPI
    {
        public status status;
        public string message { get; set; }
        public dynamic data { get; set; }
    }
    public enum status
    {
        OK = 200,
        InternalServerError = 500,
        InvalidParameter = 503,
        BadRequest = 400,
        ContentNotFound = 204,
    }
}
