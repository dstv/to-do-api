﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POCWebApp.DBContext.Models
{
    public class ApiResult<T>
    {
       
        public ApiResult(T result)
        {
            Result = result;
        }
        public string Error { get; set; }
        public T Result { get; set; }
    }
}
